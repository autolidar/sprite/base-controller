#!/usr/bin/env python2

import math
import rospy
import tf2_ros

from ackermann_msgs.msg import AckermannDrive
from gazebo_msgs.msg import ModelStates
from geometry_msgs.msg import Quaternion, TransformStamped, Twist
from nav_msgs.msg import Odometry


def convert_trans_rot_vel_to_steering_angle(v, omega, wheelbase):
    if omega == 0 or v == 0:
        return 0

    radius = v / omega
    return math.atan(wheelbase / radius)


def handle_cmd_vel(data):
    global wheelbase, ackermann_cmd_pub

    v = data.linear.x
    steering = convert_trans_rot_vel_to_steering_angle(v, data.angular.z, wheelbase)

    msg = AckermannDrive()
    msg.steering_angle = steering
    msg.speed = v

    ackermann_cmd_pub.publish(msg)


def handle_model_states(msg):
    global odom_broadcaster, odom_pub
    current_time = rospy.Time.now()

    car_pos = msg.name.index("ackermann_vehicle")

    odom_trans = TransformStamped()
    odom_trans.header.stamp = current_time
    odom_trans.header.frame_id = "odom"
    odom_trans.child_frame_id = "base_link"

    odom_trans.transform.translation = msg.pose[car_pos].position
    odom_trans.transform.rotation = msg.pose[car_pos].orientation

    odom_broadcaster.sendTransform(odom_trans)

    odom = Odometry()
    odom.header.stamp = current_time
    odom.header.frame_id = "odom"
    odom.child_frame_id = "base_link"

    odom.pose.pose = msg.pose[car_pos]
    odom.twist.twist = msg.twist[car_pos]

    odom_pub.publish(odom)


def simulation():
    global odom_broadcaster, odom_pub
    global wheelbase, ackermann_cmd_pub
    rospy.init_node("sprite_base_controller")

    wheelbase = rospy.get_param("~wheelbase", 1.0)

    rospy.Subscriber("/cmd_vel", Twist, handle_cmd_vel, queue_size=1)
    rospy.Subscriber("/gazebo/model_states", ModelStates, handle_model_states)
    ackermann_cmd_pub = rospy.Publisher("/ackermann_cmd", AckermannDrive, queue_size=1)
    odom_pub = rospy.Publisher("odom", Odometry, queue_size=1)
    odom_broadcaster = tf2_ros.TransformBroadcaster()

    rospy.spin()


if __name__ == "__main__":
    simulation()
